FROM odoo:13

USER root

COPY odoo.conf /etc/odoo/odoo.conf

RUN apt update
RUN apt install git -y

COPY requirements.txt /opt/requirements.txt

RUN pip3 install  --no-cache-dir -U pip
RUN pip3 install  --no-cache-dir -r /opt/requirements.txt

RUN git clone --depth=1 https://elotrojavi:CzJRxHTi5H5O1jbnrK5M@gitlab.com/librecoop/ecooo/domatix-addons.git /mnt/domatix-addons